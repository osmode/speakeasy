SpeakEasy - writing history together
By: Omar Metwally 

omar.metwally@gmail.com
omarmetwally.wordpress.com
omarmetwally.quora.com
logisome.com
@osmode

How it works:
SpeakEasy is an Ethereum contract to create a decentralized authorship platform. It allows the crowd to generate sentences,
word by word, by voting on the next word every 15 minutes.


